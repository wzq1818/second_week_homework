# 第二周作业

#### 介绍
飞机大战实现了要求的三个功能，但是需要说明一下。
1、敌机子弹效果，敌机出场自带子弹且没有对敌机子弹速度进行控制
2、爆炸效果，四张爆炸效果图片循环一遍，但计算机执行速度太快，只能看到一瞬间的效果
3、碰撞检测，所提交代码检测敌机子弹碰撞英雄飞机，限定100发子弹，超过100发子弹游戏结束
不知道以上功能代码是否满足作业要求，请老师指点。

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)