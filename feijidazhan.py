import pygame
from pygame.locals import *
import time,random

class HeroPlane:
	def __init__(self,a):
		self.x=203
		self.y=400
		self.screen=a
		self.image=pygame.image.load('./images/me.png')
		self.bullet_list=[]#2、飞机类里面定义子弹列表

	def display(self):
		for bullet in self.bullet_list:#3、在display里面遍历子弹列表
			bullet.display()#在子弹列表里将子弹对象display出来
			if bullet.m():#对象display出来后让子弹对象move
				self.bullet_list.remove(bullet)
		self.screen.blit(self.image,(self.x,self.y))#这个是display出飞机
	def move_left(self):
		self.x-=5
		if self.x<=0:
			self=0
	def move_right(self):
		self.x+=5
		if self.x>406:
			self=40
	def fire(self):#4、定义开火函数使子弹对象初始化出来并添加到子弹列表里
		self.bullet_list.append(Bullet(self.screen,self.x,self.y))
		print(len(self.bullet_list))

	def herolife(self,eplane):
		for h in eplane.embullet_list:#敌机子弹与英雄飞机碰撞检测
			#print(h)子弹对象存在
			if h.x>self.x+12 and h.x<self.x+92 and h.y>self.y-60 and h.y<self.y-20:
				#print('碰撞检测执行了')
				eplane.embullet_list.remove(h)
				return True

class Bullet:#1、先定义子弹类
	def __init__(self,c,x,y):
		self.x=x+51
		self.y=y
		self.screen=c
		self.image=pygame.image.load('./images/pd.png')
	def display(self):
		self.screen.blit(self.image,(self.x,self.y))
	def m(self):
		self.y-=10
		if self.y<-20:
			return True

class EnemyPlane:
	def __init__(self, d):
		self.x=random.choice(range(406))
		self.y=-75
		self.screen=d
		self.image=pygame.image.load('./images/e'+str(random.choice(range(3)))+'.png')
		self.embullet_list=[]


	def display(self):
		self.screen.blit(self.image,(self.x,self.y))
	def embudisplay(self):
		for embullet in self.embullet_list:
			embullet.display()
			if embullet.em():
				self.embullet_list.remove(embullet)
				#time.sleep(0.05)
	def move(self,hplane):
		self.y+=4
		for o in hplane.bullet_list:#碰撞检测
			if o.x>self.x+12 and o.x<self.x+92 and o.y>self.y+20 and o.y<self.y+60:
				hplane.bullet_list.remove(o)
				return True
	def emf(self):
		self.embullet_list.append(Embullet(self.screen,self.x,self.y))
		print('敌机子弹对象:',len(self.embullet_list))

class Embullet:
	def __init__(self,e,x,y):
		self.x=x+52
		self.y=y+30
		self.screen=e
		self.image=pygame.image.load('./images/epd.png')
	def display(self):
		self.screen.blit(self.image,(self.x,self.y))
	def em(self):
		self.y+=10
		if self.y>548:
			return True

def baozha(diji):
	bomb=['bomb0','bomb1','bomb2','bomb3']
	for i in range(len(bomb)):
		diji.image=pygame.image.load('./images/bomb'+str(i)+'.png')
		diji.screen.blit(diji.image,(diji.x,diji.y))
		print('爆炸效果'+str(i))#爆炸效果可以输出，但是循环太快了，sleep一下又影响整个循环卡顿
def key_control(b):
	for event in pygame.event.get():
		if event.type==QUIT:
			print('exit了')
			exit()

	pressed_keys=pygame.key.get_pressed()
	if pressed_keys[K_LEFT] or pressed_keys[K_a]:
		print('K_LEFT')
		b.move_left()
	elif pressed_keys[K_RIGHT] or pressed_keys[K_d]:
		print('K_RIGHT')
		b.move_right()
	if pressed_keys[K_SPACE]:
		print('space')
		b.fire()

def main():
	#窗口
	screen=pygame.display.set_mode((512,568),0,0)
	#加载
	background=pygame.image.load('./images/bg2.jpg')
	m=-968
	hero=HeroPlane(screen)			#飞机在此实例化
	enemylist=[]
	hlive=0

	while True:
		screen.blit(background,(0,m))
		m+=2
		if m>=-200:
			m=-968
		hero.display()#5飞机对象display一下
		key_control(hero)#6每循环一次执行等待下一次执行
		if random.choice(range(50))==10:
			enemylist.append(EnemyPlane(screen))#敌机的实例化

		for em in enemylist:
			em.embudisplay()
			em.emf()#（1、敌机子弹效果）
			#print('可以执行')
			em.display()
			#print('执行了')
			if em.move(hero):#hero是玩家飞机对象，用来调用其子弹列表里的子弹对象来与敌机检测
				print('移除一个敌机对象')
				enemylist.remove(em)#remove敌机对象
				#在此循环播放爆炸图片
				baozha(em)#敌机被击中即调用爆炸函数（2、爆炸效果）
			#print('执行了')
			if hero.herolife(em):
				hlive+=1
				print('被敌机击中',hlive,'次')
		if hlive>=100:#被敌机击中100次就game over(3、调用碰撞检测)
			print('GAME OVER了!!!')
			print('再见！！！')
			break
		pygame.display.update()
		time.sleep(0.04)

if __name__=='__main__':
	main()